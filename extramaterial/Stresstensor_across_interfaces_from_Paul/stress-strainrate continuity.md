Let us assume an interface between two materials with different
rheologies subject to stress. We assume simple linear (Newtonian)
viscosity with

${\dot{\epsilon }}_{\text{ij}}={\mathit{A\sigma }}_{\text{ij}}$(1)

Both the strain rate and deviatoric stress tensors are symmetric. The
same principles also apply to more complicated rheologies, such as
non-linear viscosity or anisotropy.

The straight interface bounds two half spaces occupied by materials *A*
and *B*. Each has its own stress and strain-rate state, which does not
vary in space within each material. We further assume \* that the
interface is parallel to the *x*-axis and ignore the 3^rd^ dimension,

-   that the materials are incompressible:
    ${\dot{\epsilon }}_{\text{xx}}=-{\dot{\epsilon }}_{\text{yy}}$,
-   and that there is no slip along the interface

We can now set up the following set of compatibility equations, where we
use ** for the absolute stress:

$\begin{array}{c}{\tau }_{\text{yy}}^{A}={\tau }_{\text{yy}}^{B}\\ 
{\tau }_{\text{xy}}^{A}={\tau }_{\text{yx}}^{A}={\tau }_{\text{xy}}^{B}={\tau }_{\text{yx}}^{B}\\ 
{\dot{\epsilon }}_{\text{xx}}^{A}={\dot{\epsilon }}_{\text{xx}}^{B}\end{array}$(2)

From the strain-rate compatibility (stretching/shortening parallel to
the interface identical in both materials) we get:

${\mathit{A\sigma }}_{\text{xx}}^{A}=-{\mathit{A\sigma }}_{\text{yy}}^{A}={\mathit{B\sigma }}_{\text{xx}}^{B}=-{\mathit{B\sigma }}_{\text{yy}}^{B}$(3)

We use **= **- *P* (*P* is pressure) and set the reference pressure
in material *A* to zero, giving:

${\mathit{A\tau }}_{\text{xx}}^{A}=-{\mathit{A\tau }}_{\text{yy}}^{A}=B\left({\tau }_{\text{xx}}^{B}-P\right)=-B\left({\tau }_{\text{yy}}^{B}-P\right)$(4)

This gives us the pressure \'\'P^B\'\'^ in material *B* relative to that
in material *A* (with \'\'P^A\'\'^=0):

${\mathit{A\tau }}_{\text{yy}}^{A}=B\left({\tau }_{\text{yy}}^{B}-{P}^{B}\right)\Leftrightarrow {P}^{B}={\tau }_{\text{yy}}^{B}-\frac{A}{B}{\tau }_{\text{yy}}^{A}=\left(1-\frac{A}{B}\right){\tau }_{\text{yy}}^{A}$(5)

Clearly, pressures are identical when both materials have the same
viscosity (*A*=*B*). We can now express the absolute and deviatoric
stress tensors of material *B* in terms of the stresses in material *A*
(which is at zero pressure, so we only need ${\tau }_{\text{xx}}^{A}$and
${\tau }_{\text{xy}}^{A}$):

Absolute stress tensor:
${\tau }_{\text{xy}}^{B}=\left\lbrack \begin{array}{cc}\left(\frac{2A}{B}-1\right){\tau }_{\text{xx}}^{A}&     {\tau }_{\text{xy}}^{A}\\ 
{\tau }_{\text{xy}}^{A}&    -{\tau }_{\text{xx}}^{A}\end{array}\right\rbrack$(6)

Deviatoric stress tensor:
${\sigma }_{\text{xy}}^{B}=\left\lbrack \begin{array}{cc}\frac{A}{B}{\tau }_{\text{xx}}^{A}&     {\tau }_{\text{xy}}^{A}\\ 
{\tau }_{\text{xy}}^{A}&    -\frac{A}{B}{\tau }_{\text{xx}}^{A}\end{array}\right\rbrack$(7)

Finally we consider the second invariants of the deviatoric stress
tensors:

$\begin{array}{c}{\sigma }_{\text{II}}^{A}=\frac{1}{2}\sqrt{2{\left({\tau }_{\text{xx}}^{A}\right)}^{2}+2{\left({\tau }_{\text{xy}}^{A}\right)}^{2}}\\ 
{\sigma }_{\text{II}}^{B}=\frac{1}{2}\sqrt{2{\left(\frac{A}{B}{\tau }_{\text{xx}}^{A}\right)}^{2}+2{\left({\tau }_{\text{xy}}^{A}\right)}^{2}}\end{array}$(8)

In conclusion: \* The absolute stress perpendicular to the interface and
the shear stresses parallel and perpendicular to the interface are
continuous.

-   The absolute stress parallel to the interface is not continuous.
-   The second invariants of the two materials are not equal
-   There is a pressure difference between the two materials

This holds unless the stress state is perfect simple shear parallel to
the interface.
