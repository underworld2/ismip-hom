#!/bin/bash

for f in *.py
do
	for procs in "0,1,2,3,4,5,6" "0,1,2,3,4,5" "0,1,2,3,4" "0,1,2,3" "0,1,2" "0,1" "0"      
	do
	  echo $f $procs&&
	  
	  docker run -v $PWD:/home/jovyan/workspace -w /home/jovyan/workspace --rm -it underworldcode/underworld2:latest taskset -c $procs python $f &&
	  
	  echo 'next'
	done
done

