[1;35m
 
Pressure iterations:  50
Velocity iterations:  17 (presolve)      
Velocity iterations: 480 (pressure solve)
Velocity iterations:  10 (backsolve)     
Velocity iterations: 507 (total solve)   
 
SCR RHS  setup time: 4.2823e-02
SCR RHS  solve time: 3.6897e-02
Pressure setup time: 2.0537e-03
Pressure solve time: 7.9186e-01
Velocity setup time: 4.7684e-07 (backsolve)
Velocity solve time: 1.5951e-02 (backsolve)
Total solve time   : 8.9504e-01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
