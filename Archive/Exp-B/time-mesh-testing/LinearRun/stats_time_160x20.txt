[1;35m
 
Pressure iterations:  60
Velocity iterations:  16 (presolve)      
Velocity iterations: 579 (pressure solve)
Velocity iterations:  10 (backsolve)     
Velocity iterations: 605 (total solve)   
 
SCR RHS  setup time: 8.9180e-03
SCR RHS  solve time: 1.5898e-02
Pressure setup time: 9.5725e-04
Pressure solve time: 3.9999e-01
Velocity setup time: 2.3842e-07 (backsolve)
Velocity solve time: 6.7189e-03 (backsolve)
Total solve time   : 4.3583e-01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
