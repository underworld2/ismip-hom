[1;35m
 
Pressure iterations:  47
Velocity iterations:  25 (presolve)      
Velocity iterations: 492 (pressure solve)
Velocity iterations:  13 (backsolve)     
Velocity iterations: 530 (total solve)   
 
SCR RHS  setup time: 4.0540e-01
SCR RHS  solve time: 4.3671e-01
Pressure setup time: 1.0406e-02
Pressure solve time: 7.0298e+00
Velocity setup time: 4.7684e-07 (backsolve)
Velocity solve time: 1.8252e-01 (backsolve)
Total solve time   : 8.0946e+00
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
