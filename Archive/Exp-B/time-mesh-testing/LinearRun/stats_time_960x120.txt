[1;35m
 
Pressure iterations:  48
Velocity iterations:  39 (presolve)      
Velocity iterations: 735 (pressure solve)
Velocity iterations:  26 (backsolve)     
Velocity iterations: 800 (total solve)   
 
SCR RHS  setup time: 2.9338e-01
SCR RHS  solve time: 2.0387e+00
Pressure setup time: 3.0787e-02
Pressure solve time: 3.2591e+01
Velocity setup time: 7.1526e-07 (backsolve)
Velocity solve time: 1.1773e+00 (backsolve)
Total solve time   : 3.6240e+01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
