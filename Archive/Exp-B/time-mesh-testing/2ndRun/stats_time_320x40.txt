[1;35m
 
Pressure iterations:  46
Velocity iterations:  26 (presolve)      
Velocity iterations: 577 (pressure solve)
Velocity iterations:  13 (backsolve)     
Velocity iterations: 616 (total solve)   
 
SCR RHS  setup time: 2.6757e-02
SCR RHS  solve time: 1.0584e-01
Pressure setup time: 3.5226e-03
Pressure solve time: 1.8756e+00
Velocity setup time: 4.7684e-07 (backsolve)
Velocity solve time: 4.1070e-02 (backsolve)
Total solve time   : 2.0638e+00
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
