[1;35m
 
Pressure iterations:  45
Velocity iterations:  29 (presolve)      
Velocity iterations: 594 (pressure solve)
Velocity iterations:  23 (backsolve)     
Velocity iterations: 646 (total solve)   
 
SCR RHS  setup time: 7.8156e-01
SCR RHS  solve time: 8.7695e-01
Pressure setup time: 1.7383e-02
Pressure solve time: 1.4939e+01
Velocity setup time: 7.1526e-07 (backsolve)
Velocity solve time: 5.8136e-01 (backsolve)
Total solve time   : 1.7242e+01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
