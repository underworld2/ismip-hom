[1;35m
 
Pressure iterations:  79
Velocity iterations: 5043 (presolve)      
Velocity iterations: 121743 (pressure solve)
Velocity iterations: 1139 (backsolve)     
Velocity iterations: 127925 (total solve)   
 
SCR RHS  setup time: 6.0916e-03
SCR RHS  solve time: 1.7206e+00
Pressure setup time: 3.5572e-04
Pressure solve time: 4.2853e+01
Velocity setup time: 2.3842e-07 (backsolve)
Velocity solve time: 4.6378e-01 (backsolve)
Total solve time   : 4.5046e+01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
