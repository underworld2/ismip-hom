[1;35m
 
Pressure iterations:  45
Velocity iterations:  14 (presolve)      
Velocity iterations: 373 (pressure solve)
Velocity iterations:   8 (backsolve)     
Velocity iterations: 395 (total solve)   
 
SCR RHS  setup time: 1.6358e-02
SCR RHS  solve time: 1.5002e-02
Pressure setup time: 6.9213e-04
Pressure solve time: 2.8970e-01
Velocity setup time: 2.3842e-07 (backsolve)
Velocity solve time: 5.6393e-03 (backsolve)
Total solve time   : 3.3012e-01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
