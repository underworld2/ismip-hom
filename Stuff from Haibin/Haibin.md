Hi Till,

I have updated the perfectly body-fit mesh which is defined in lines 100-120 (function named as mesh_deform_Ind)

The other change is the stress evaluation which is now defined on the subMesh (lines434-436).

```
meshStressTensor = uw.mesh.MeshVariable(mesh.subMesh, 3 )
projectorStress = uw.utils.MeshVariable_Projection( meshStressTensor, stressMapFn, type=0 )
projectorStress.solve()
```

It is better to define the gradient (strain rate or stress) in the interior of the element (better to be on the superconvergent point rather than the node). The subMesh like the pressureField can be a good position to check the stress. If the stress is projected to a node, it will be smoothed around the interface.
However, if it is projected to the superconvergent point, it will only use the information in that element and won't cause a mixing effect.

With the stress defined in the convergent point and the body-fit mesh, you can evaluate the stress very close (~0.5m ) to the interface. 

Cheers, 
