#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import warnings
warnings.filterwarnings("ignore")
import underworld as uw
from underworld import function as fn
import glucifer

from scipy import spatial
import numpy as np
import math
import os,csv

from underworld.scaling import units as u
from underworld.scaling import non_dimensionalise as nd


tempMin = 273.*u.degK 
tempMax = (1400.+ 273.)*u.degK
bodyforce = 3300 * u.kilogram / u.metre**3 * 9.81 * u.meter / u.second**2
velocity = 2*u.meter/u.year

KL = 100*u.meter
Kt = KL/velocity
KT = tempMax 
KM = bodyforce * KL**2 * Kt**2
K  = 1.*u.mole
lengthScale = 100e3

scaling_coefficients = uw.scaling.get_coefficients()
scaling_coefficients["[length]"] = KL
scaling_coefficients["[time]"] = Kt
scaling_coefficients["[mass]"]= KM
scaling_coefficients["[temperature]"] = KT
scaling_coefficients["[substance]"] = K

gravity = nd(9.81 * u.meter / u.second**2)
R = nd(8.3144621 * u.joule / u.mole / u.degK)

# Keep Node_based, Element_based, Gauss_based, Nearest False
# only use meshdeform to set the refined mesh at the interface 
meshdeform = True
# Node-based method
Node_based = False
# Element-based method
Element_based = False
# Gauss-quadrature-point-based method
Gauss_based = False
# GaussHM True: Harmonic mean; False: Arithmetic mean. for the Gauss-quadrature-point-based method
Hm = False
# Nearest neighbour method.This is used to test whether the Kdtree method does the same as UW
Nearest = False
# False:Q1 element; True: Q2 element
ElementQ2 = False 

Rad = 2

vel_factor = nd(1.*u.meter/u.year)
stress_factor = nd(1.*u.pascal)
vis_factor = nd(1.*u.pascal*u.second)

# for shift_factor in Shift_factor :    
#     for results in Res_array:

Res = 64
outputPath = os.path.join(os.path.abspath("."),"IceSheet_none_Res48/")

if uw.rank() == 0:
    if not os.path.exists(outputPath):
        os.makedirs(outputPath)
uw.barrier()

resX = 2*Res
resY = Res
materialA = 1
materialB = 0
materialAB = 2


step = 0

if ElementQ2 ==True:
    elementType = "Q2/dpc1"  
else:
    elementType = "Q1/dQ0" 

mesh = uw.mesh.FeMesh_Cartesian( elementType = (elementType), 
                                 elementRes  = ( resX, resY)
                                  ) 


minX  = nd(0.*u.meter)
maxX  = nd(5000.*u.meter)
minY  = nd(-1000.*u.meter)
maxY  = nd(1000.*u.meter)

mesh = uw.mesh.FeMesh_Cartesian( elementType = (elementType), 
                             elementRes  = ( resX, resY), 
                             minCoord    = ( minX, minY), 
                             maxCoord    = ( maxX, maxY),
                             periodic    = [True,False]) 
def mesh_deform_Ind(section,fixPoint_index,fixPoint,mi):
    section[fixPoint_index] = fixPoint
    seqN = len(section)
    # fixPoint_index (int): specify the index of the section to be at the place need to be refined
    # fixPoint: the position to be refined
    # mi: representing the gradient of mesh resolution; the larger mi, the larger gradient
    for index in xrange(len(section)):
        maxCoord = np.max(section)
        minCoord = np.min(section)
        if  index < fixPoint_index:
            section[index] = minCoord + index*(fixPoint-minCoord)/fixPoint_index            
            zz_sqrt = (fixPoint-section[index])**mi
            zz_sqrt_max = (fixPoint-minCoord)**mi
            section[index] = fixPoint-(fixPoint-section[index])*zz_sqrt/zz_sqrt_max

        if  index > fixPoint_index:
            section[index] = fixPoint + (index-fixPoint_index)*(maxCoord-fixPoint)/(seqN-fixPoint_index-1)
            zz_sqrt = (section[index]-fixPoint)**mi
            zz_sqrt_max = (maxCoord-fixPoint)**mi
            section[index] =fixPoint+(section[index]-fixPoint)*zz_sqrt/zz_sqrt_max        
    return section

def mesh_deform(section,fixPoint,mi):
    # fixPoint: the position to be refined
    # mi: representing the gradient of mesh resolution; the larger mi, the larger gradient
    # generally, mi ragnes from 0.1 to 3, depends on the resolution 
    for index in xrange(len(section)):
        maxCoord = np.max(section)
        minCoord = np.min(section)
        if  section[index]<=fixPoint:
            zz_sqrt = (fixPoint-section[index])**mi
            zz_sqrt_max = (fixPoint-minCoord)**mi
            section[index] = fixPoint-(fixPoint-section[index])*zz_sqrt/zz_sqrt_max

        if  section[index]>=fixPoint:
            zz_sqrt = (section[index]-fixPoint)**mi
            zz_sqrt_max = (maxCoord-fixPoint)**mi
            section[index] =fixPoint+(section[index]-fixPoint)*zz_sqrt/zz_sqrt_max        
    return section

     

velocityField    = uw.mesh.MeshVariable( mesh=mesh,         nodeDofCount=mesh.dim )
pressureField    = uw.mesh.MeshVariable( mesh=mesh.subMesh, nodeDofCount=1 )
pressureField_smoth    = uw.mesh.MeshVariable( mesh=mesh.subMesh, nodeDofCount=1 )
stressField    = uw.mesh.MeshVariable( mesh=mesh, nodeDofCount=2 )


velocityField.data[:] = [0.,0.]
pressureField.data[:] = 0.
stressField.data[:] = [0.,0.]

#  Boundary conditions
iWalls = mesh.specialSets["MinI_VertexSet"] + mesh.specialSets["MaxI_VertexSet"]
jWalls = mesh.specialSets["MinJ_VertexSet"] + mesh.specialSets["MaxJ_VertexSet"]
base   = mesh.specialSets["MinJ_VertexSet"]
top    = mesh.specialSets["MaxJ_VertexSet"]

dx = (maxY-minY)/Res
shift = 0.5*shift_factor*(maxY-minY)/Res


velocityBCs = uw.conditions.DirichletCondition( variable        = velocityField, 
                                                   indexSetsPerDof = (base,base) )




swarm  = uw.swarm.Swarm( mesh=mesh,particleEscape=True )
pop_control = uw.swarm.PopulationControl(swarm,aggressive=True,particlesPerCell=16)        
vis_mat = swarm.add_variable( dataType="double", count=1 )
materialVariable = swarm.add_variable( dataType="int", count=1 )


# swarm2 is used only in the Element_based method
swarm2  = uw.swarm.Swarm( mesh=mesh,particleEscape=True )
mat2 = swarm2.add_variable( dataType="int", count=1 )
vis_gausse = swarm2.add_variable( dataType="double", count=1 )


swarmLayout = uw.swarm.layouts.PerCellSpaceFillerLayout( swarm=swarm, particlesPerCell=16 )
swarm.populate_using_layout( layout=swarmLayout )


picswarm = uw.swarm.VoronoiIntegrationSwarm(swarm)
picswarm.repopulate()

if (meshdeform == True):
    with mesh.deform_mesh():
        
        for index in range(resX+1):
        #index = 0    
            start_x = dx*index
            interface_y =  5.*np.sin(start_x*(np.pi*2.)/(maxX-minX))
            #print start_x,interface_y
            ind = np.where(abs(mesh.data[:,0]-start_x)<0.01*dx)
            #mesh.data[ind[0],1] = mesh_deform(mesh.data[ind[0],1],interface_y,2.0)
            mesh.data[ind[0],1] = mesh_deform_Ind(mesh.data[ind[0],1],resY/2,interface_y,0.2)
    # make the swarm belong the element that is the same as the in the regular mesh
    with swarm.deform_swarm():
        swarm.particleCoordinates.data[:] = uw.function.input().evaluate(picswarm) 
        
if ElementQ2 == True:
    swarmLayout2 = uw.swarm.layouts.PerCellGaussLayout( swarm=swarm2, gaussPointCount=5  )
else:
    swarmLayout2 = uw.swarm.layouts.PerCellGaussLayout( swarm=swarm2, gaussPointCount=3  )

swarm2.populate_using_layout( layout=swarmLayout2 )

#set up the model geometry for material 1 and 2

coord = fn.input()
interface = nd(500.*u.meter)*fn.math.sin(coord[0]*(2.*np.pi)/(maxX-minX))

condition_fault = [(coord[1]>interface,materialB),
                       (True, materialA)]   

materialVariable.data[:] = fn.branching.conditional(condition_fault).evaluate(swarm)

mat2.data[:] = fn.branching.conditional(condition_fault).evaluate(swarm2)  

# find and index elements that contain mixed materials of materialA and materialB
ind1 = np.where(materialVariable.data[:]==materialA)
ind0 = np.where(materialVariable.data[:]==materialB)
index_common = np.intersect1d(swarm.owningCell.data[ind1[0]],swarm.owningCell.data[ind0[0]])



if Element_based == True: # Element_based method
    #print index_common
    for index, coord in enumerate(swarm.particleCoordinates.data):
        if swarm.owningCell.data[index] in index_common :
            #if materialVariable.data[index] == materialB:
            materialVariable.data[index] = materialAB    
    print "Element_based method is called"


densityMap = { materialA: nd(   2700. * u.kilogram / u.metre**3),
               materialB: nd(   910. * u.kilogram / u.metre**3),
               materialAB: nd(  1800. * u.kilogram / u.metre**3) }

densityFn = fn.branching.map( fn_key=materialVariable, mapping=densityMap )

alpha = 0.5/180.*np.pi
# And the final buoyancy force function. For the benchmark model, no bouyancy is considered.
z_hat = ( 1*np.sin(alpha), -1.0*np.cos(alpha) )      
buoyancyFn = densityFn * z_hat * gravity



strainRateFn = fn.tensor.symmetric( velocityField.fn_gradient )
strainRate_2ndInvariantFn = fn.tensor.second_invariant(strainRateFn)+nd(1e-18/u.second)

n_factor = 3.
A_factor = 1e-16*nd(1.*u.pascal**(-n_factor)*(1./u.year))
Power_law = 0.5*fn.math.pow(strainRate_2ndInvariantFn,(1-n_factor)/n_factor)*fn.math.pow(A_factor, -1./n_factor)
    
viscosityA = nd(1e22*u.pascal*u.second)
viscosityB = Power_law #1e-6*viscosityA   #nd(3e19*u.pascal*u.second)#      
mappingDictViscosity   = {  
                          materialA: viscosityA,
                          materialB: viscosityB,
                          materialAB:2./(1./viscosityA+1./viscosityB)}
viscosityMapFn  = fn.branching.map( fn_key=materialVariable, mapping=mappingDictViscosity )

vis_mat.data[:] = viscosityMapFn.evaluate(swarm)

if Node_based == True:
    meshViscosityReciprocal = uw.mesh.MeshVariable( mesh=mesh, nodeDofCount=1 )
    meshViscosityProjector = uw.utils.MeshVariable_Projection( meshViscosityReciprocal, 1./viscosityMapFn, type=0 )
    meshViscosityProjector.solve()    
    vis_harmonic = 1./meshViscosityReciprocal
    viscosityFn0 = vis_harmonic
    print "Node_based method is called"

elif Gauss_based == True:
    import sklearn.neighbors as kdt
    vis_gausse.data[:] = fn.branching.map(fn_key=mat2, mapping=mappingDictViscosity ).evaluate(swarm2)

    tree = kdt.KDTree(swarm.particleCoordinates.data)

    delta = min((maxX-minX)/resX,(maxY-minY)/resY)*Rad

    for index, coord in enumerate(swarm2.particleCoordinates.data):
        if swarm2.owningCell.data[index] in index_common :


            balls = tree.query_radius([swarm2.particleCoordinates.data[index]], delta,return_distance=True)
            ind = balls[0]
            dis = balls[1]


            if Hm == True:
                #Harmonic mean
                dis_inverse = 1./dis[0] 
                dis_sum = np.sum(dis_inverse)
                mat = 1./vis_mat.data[ind[0]][:,0]*dis_inverse
                vis_gausse.data[index] = dis_sum/np.sum(mat)
            else:
                #Arithmetic mean
                dis_inverse = 1./dis[0] 
                dis_sum = np.sum(dis_inverse)
                mat = vis_mat.data[ind[0]][:,0]*dis_inverse/dis_sum
                vis_gausse.data[index] = np.sum(mat)

    viscosityFn0 = vis_gausse    

    print "Gauss_based method is called"

elif Nearest == True:
    import sklearn.neighbors as kdt
    vis_gausse.data[:] = fn.branching.map(fn_key=mat2, mapping=mappingDictViscosity ).evaluate(swarm2)
    tree = kdt.KDTree(swarm.particleCoordinates.data)

    delta = min((maxX-minX)/resX,(maxY-minY)/resY)*Rad

    for index, coord in enumerate(swarm2.particleCoordinates.data):
        if swarm2.owningCell.data[index] in index_common :

            dist, ind = tree.query([swarm2.particleCoordinates.data[index]], k=1)                # doctest: +SKIP
            vis_gausse.data[index] = vis_mat.data[ind]

    viscosityFn0 = vis_gausse   
    print "nearest is called"
else:
    viscosityFn0 = viscosityMapFn
    print "default is called"

strainRateFn = fn.tensor.symmetric( velocityField.fn_gradient )
strainRate_2ndInvariantFn = fn.tensor.second_invariant(strainRateFn)#+nd(1e-18/u.second)   


viscosityFn =viscosityFn0 

stressMapFn = 2.0 * viscosityFn * strainRateFn

allStress =  fn.tensor.second_invariant(stressMapFn)

stokes = uw.systems.Stokes(velocityField = velocityField, 
                           pressureField = pressureField,
                           conditions    = [velocityBCs,],
                           fn_viscosity  = viscosityFn,
                           fn_bodyforce  = buoyancyFn) 


solver = uw.systems.Solver( stokes )
solver.set_inner_method('mumps') 
solver.set_penalty(1e3) 

surfaceArea = uw.utils.Integral(fn=1.0,mesh=mesh, integrationType='surface', surfaceIndexSet=top)
surfacePressureIntegral = uw.utils.Integral(fn=pressureField, mesh=mesh, integrationType='surface', surfaceIndexSet=top)

def nonLinearSolver(step, nl_tol=1e-2, nl_maxIts=10):
    # a hand written non linear loop for stokes, with pressure correction

    er = 1.0
    its = 0                      # iteration count
    v_old = velocityField.copy() # old velocityField 

    while er > nl_tol and its < nl_maxIts:

        v_old.data[:] = velocityField.data[:]
        solver.solve(nonLinearIterate=False)

        # pressure correction for bob (the feed back pressure)

       # minP.evaluate(mesh.subMesh)
        (area,) = surfaceArea.evaluate()
        (p0,) = surfacePressureIntegral.evaluate() 
        pressureField.data[:] -= (p0 / area)
        #pressureField.data[:] += nd(1e7*u.pascal)
        if Gauss_based == True:
            vis_gausse.data[:] = fn.branching.map(fn_key=mat2, mapping=mappingDictViscosity ).evaluate(swarm2)
            tree = kdt.KDTree(swarm.particleCoordinates.data)
            delta = min((maxX-minX)/resX,(maxY-minY)/resY)*Rad

            for index, coord in enumerate(swarm2.particleCoordinates.data):
                if swarm2.owningCell.data[index] in index_common :


                    balls = tree.query_radius([swarm2.particleCoordinates.data[index]], delta,return_distance=True)
                    ind = balls[0]
                    dis = balls[1]


                    if Hm == True:
                        #Harmonic mean
                        dis_inverse = 1./dis[0] 
                        dis_sum = np.sum(dis_inverse)
                        mat = 1./vis_mat.data[ind[0]][:,0]*dis_inverse
                        vis_gausse.data[index] = dis_sum/np.sum(mat)
                    else:
                        #Arithmetic mean
                        dis_inverse = 1./dis[0] 
                        dis_sum = np.sum(dis_inverse)
                        mat = vis_mat.data[ind[0]][:,0]*dis_inverse/dis_sum
                        vis_gausse.data[index] = np.sum(mat)

            viscosityFn0 = vis_gausse 
            viscosityFn = viscosityFn0


        # calculate relative error
        absErr = uw.utils._nps_2norm(velocityField.data-v_old.data)
        magT   = uw.utils._nps_2norm(v_old.data)
        er = absErr/magT
        if uw.rank()==0.:
            print "tolerance=", er,"iteration times=",its
        uw.barrier()

        its += 1
                
def pressure_calibrate():
    
    (area,) = surfaceArea.evaluate()
    (p0,) = surfacePressureIntegral.evaluate()
    offset = p0/area
    pressureField.data[:] -= offset


import time
t0= time.clock()

#nonLinearSolver(step, nl_tol=1e-4, nl_maxIts=30)
solver.solve(nonLinearIterate=True,nonLinearTolerance=1e-4,                nonLinearMaxIterations=30,callback_post_solve = pressure_calibrate)
t1= time.clock()

print t1-t0

meshAllStress = uw.mesh.MeshVariable( mesh, 1 )
projectorStress = uw.utils.MeshVariable_Projection( meshAllStress, allStress, type=0 )
projectorStress.solve()

meshStressTensor = uw.mesh.MeshVariable(mesh.subMesh, 3 )
projectorStress = uw.utils.MeshVariable_Projection( meshStressTensor, stressMapFn, type=0 )
projectorStress.solve()

meshViscosity = uw.mesh.MeshVariable( mesh, 1 )
projectorViscosity = uw.utils.MeshVariable_Projection( meshViscosity,vis_mat, type=0 )
projectorViscosity.solve() 


mesh.save(outputPath+"mesh"+str(step).zfill(4))
meshViscosity.save(outputPath+"meshViscosity"+str(step).zfill(4))
meshAllStress.save(outputPath+"meshAllStress"+str(step).zfill(4)) 
meshStressTensor.save(outputPath+"meshStressTensor"+str(step).zfill(4)) 
swarm.save(outputPath+"swarm"+str(step).zfill(4))
swarm2.save(outputPath+"swarm2"+str(step).zfill(4))
velocityField.save(outputPath+"velocityField"+str(step).zfill(4))     
pressureField.save(outputPath+"pressureField"+str(step).zfill(4))
materialVariable.save(outputPath+"materialVariable"+str(step).zfill(4)) 

dicMesh = { 'elements' : mesh.elementRes, 
            'minCoord' : mesh.minCoord,
            'maxCoord' : mesh.maxCoord}

fo = open(outputPath+"dicMesh"+str(step).zfill(4),'w')
fo.write(str(dicMesh))
fo.close()


# In[ ]:


# minbox = (minX,minY)
# maxbox = (maxX,maxY) 
minbox = (6,3.15,)#
maxbox = (6.5,3.5)# 
fig = glucifer.Figure( figsize=(1600,1800),quality=3,clipmap=False, xmin=minbox[0], xmax=maxbox[0], ymin=minbox[1], ymax=maxbox[1],
                        boundingBox=(minbox,maxbox),margin = 200, rulers = True, rulerticks= 5 )
surf=glucifer.objects.Points(swarm, materialVariable,pointSize=5,discrete=True,
                            colours="jet" ,opacity=0.5,colourBar = True)
fig.append(surf ) 

fig.append(glucifer.objects.Mesh( mesh ))
#fig.append(glucifer.objects.VectorArrows( mesh,velocityField,\
#                                             scaling=8e-1,resolution=[16,20,1]))
#fig.script('scale y 4') 
   
surf.colourBar["position"] = 480
surf.colourBar["align"] = "bottom"
surf.colourBar["size"] = [0.4,0.02]
#fig.save('/home/haibin/Desktop/'+'ExpB_L5_mesh'+'.jpg') 
fig.show()


# In[ ]:


# outputPath = os.path.join(os.path.abspath("."),"IceSheet_meshDeform/")
# pr = uw.mesh.MeshVariable( mesh=mesh.subMesh, nodeDofCount=1 )
# pr.load(outputPath+'pressureField'+str(step).zfill(4))

minbox = (minX,minY)#
maxbox = (maxX,maxY) #

fig = glucifer.Figure(title="Stress_xy",figsize=(1600,800),quality=3,
               xmin=minbox[0], xmax=maxbox[0], ymin=minbox[1], ymax=maxbox[1],
             boundingBox=(minbox,maxbox),margin = 200, rulers = True )

surf = glucifer.objects.Surface( mesh,fn.math.log10(fn.math.abs(meshAllStress)/stress_factor),colours="red white blue"  )  #
#surf = glucifer.objects.Surface( mesh,(pressureField)/stress_factor,colours="jet"  )  #
#contours = glucifer.objects.Contours(mesh,(pressureField/stress_factor),interval=10000,colours='jet')
#fig.append(glucifer.objects.VectorArrows( mesh,velocityField,\
#                                             scaling=8e-1,resolution=[16,20,1]))
fig.append(surf)
#fig.append(contours)

fig.script('scale x 1')
   
# surf.colourBar["position"] = 480
# surf.colourBar["align"] = "bottom"
# surf.colourBar["size"] = [0.4,0.02]
fig.show()
    


# In[ ]:


import matplotlib.pyplot as plt
Res_n = 100
L = 50
x = np.linspace(0,L,Res_n)
y1 = 5.*np.sin(2.*np.pi*x/L) - 0.2
y2 = 5.*np.sin(2.*np.pi*x/L) + 0.7
y0 = 10.*np.ones([1,Res_n])

Base_Ice1 = np.zeros([Res_n,2])
Base_Ice2 = np.zeros([Res_n,2])

Top_Ice = np.zeros([Res_n,2])

Base_Ice1[:,0] = x
Base_Ice2[:,0] = x
Base_Ice1[:,1] = y1
Base_Ice2[:,1] = y2
Top_Ice[:,0] = x
Top_Ice[:,1] = y0
vel_top = velocityField.evaluate(Top_Ice)

# outputPath = os.path.join(os.path.abspath("."),"IceSheet_Hm4/")
# mesh = uw.mesh.FeMesh_Cartesian( elementType = (elementType), 
#                                   elementRes  = ( resX, resY), 
#                                   ) 
# mesh.load(outputPath+"mesh"+str(step).zfill(4))
# meshAllStress = uw.mesh.MeshVariable( mesh, 1 )
# meshAllStress.load(outputPath+"meshAllStress"+str(step).zfill(4))

sigmaXY_base1 = meshStressTensor[2].evaluate(Base_Ice1)
sigmaXY_base2 = meshStressTensor[2].evaluate(Base_Ice2)
vel_factor = nd(1.*u.meter/u.year)

container = np.load('/home/haibin/Desktop/'+'ExpB_L5_Res48.npz')

Vel0 = container['vel'][:,0]
Vel1 = container['vel'][:,1]
Stress1 = container['stress1']
Stress2 = container['stress2']

 
fig,axes=plt.subplots(1,2,figsize=(15,5),dpi=200)
ax1=axes[0]
ax1.plot(x,vel_top[:,0]/vel_factor,'bo',label='Horizontal')
#ax1.plot(x,vel_top[:,1]/vel_factor,'ro',label='Vertical')
ax1.plot(x,Vel0/vel_factor,'b',label='Horizontal')
#ax1.plot(x,Vel1/vel_factor,'r',label='Vertical')
ax1.legend()
ax1.set_xlabel('Length (100 m)')
ax1.set_ylabel('Surface V (m/year)')
ax2=axes[1]
#ax2.plot(x,(sigmaXY_base1/stress_factor),'bo',label='-5 m')
ax2.plot(x,(sigmaXY_base2/stress_factor),'ro',label='+10 m')
#ax2.plot(x,(sigmaXY_base2+sigmaXY_base1)/2./stress_factor,'k',label='+2 mean')
#ax2.plot(x,(Stress1/stress_factor),'b',label='-2 m')
ax2.plot(x,(Stress2/stress_factor),'r',label='+10 m')
ax2.legend()
ax2.set_xlabel('Length (km)')
ax2.set_ylabel('$\sigma_{xy} (Pa)$')
#np.savez('/home/haibin/Desktop/'+'ExpB_L160_Res48.npz', x=x, vel=vel_top,stress1=sigmaXY_base1,stress2=sigmaXY_base2)
#plt.savefig('/home/haibin/Desktop/'+'ExpB_L5'+'.jpg',dpi=300) 
plt.show()


# In[ ]:


import matplotlib as mpl
from matplotlib import cm
from matplotlib.ticker import MultipleLocator
class MplColorHelper:

    def __init__(self, cmap_name, start_val, stop_val):
        self.cmap_name = cmap_name
        self.cmap = plt.get_cmap(cmap_name)
        self.norm = mpl.colors.Normalize(vmin=start_val, vmax=stop_val)
        self.scalarMap = cm.ScalarMappable(norm=self.norm, cmap=self.cmap)

    def get_rgb(self, val):
        return self.scalarMap.to_rgba(val)
    


# In[ ]:


Name = [5,10,20,40,80,160]
LineStyle = ["-","-.","-.","-.","-.","-"]
COL = MplColorHelper('viridis', 0, len(Name))
Res_n = 100
x = np.linspace(0,1,Res_n)
fig,axes=plt.subplots(1,2,figsize=(15,5),dpi=300)
for i in range(len(Name)):

    container = np.load('/home/haibin/Desktop/'+'ExpB_L'+str(Name[i])+'_Res48'+'.npz')
    vel = container['vel'][:,0]
    Stress = container['stress2']
    ax0 = axes[0]
    ax0.plot(x,vel/vel_factor,label=str(Name[i]),color=COL.get_rgb(i),linestyle=LineStyle[i],linewidth=2) 
    ax1 = axes[1]
    ax1.plot(x,Stress/stress_factor/1000.,label=str(Name[i]),color=COL.get_rgb(i),linestyle=LineStyle[i],linewidth=2)
ax0.legend(ncol=2,loc='upper left') 
ax1.legend(ncol=2,loc='upper left')  

ax0.set_xlabel('Normalized length',fontsize=14)
ax0.set_ylabel('$Vx\ (m\  a^{-1})$',fontsize=14)
ax1.set_xlabel('Normalized length',fontsize=14)
ax1.set_ylabel('$Shear\ stress\ (kPa)$',fontsize=14)
#ax0.tick_params(which='minor',axis='both')
ax0.tick_params(axis='both',labelsize=14)
ax1.tick_params(axis='both',labelsize=14)
ax0.grid(axis="y", which="both",linestyle=':',linewidth=0.5)
ax1.grid(axis="y", which="both",linestyle=':',linewidth=0.5)
#ax1.tick_params(axis='y', which='minor', labelsize=12)
# ax1.get_xaxis().set_tick_params(which='minor', size=1)
# ax1.get_xaxis().set_tick_params(which='minor', width=1) 
#plt.savefig('/home/haibin/Desktop/'+'ExpB'+'.jpg',dpi=300) 
plt.show()

