#!/bin/bash

HOME=$PWD

echo $HOME

for f in {0..5}
do

  echo $f &&
  
  docker run -v "$PWD":/home/jovyan/workspace -w /home/jovyan/workspace --rm -it underworldcode/underworld2:latest python experiment_C-3D-19.12.-no-penalty-current-optimum-HiRes.py $f &&
  
  echo 'next'

  cd "$HOME"
  
done
