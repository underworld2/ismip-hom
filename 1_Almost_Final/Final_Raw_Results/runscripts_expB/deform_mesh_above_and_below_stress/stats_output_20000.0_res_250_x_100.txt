[1;35m
 
Pressure iterations:  45
Velocity iterations:  18 (presolve)      
Velocity iterations: 624 (pressure solve)
Velocity iterations:  13 (backsolve)     
Velocity iterations: 655 (total solve)   
 
SCR RHS  setup time: 2.4595e-01
SCR RHS  solve time: 2.5792e-01
Pressure setup time: 7.0717e-03
Pressure solve time: 7.1438e+00
Velocity setup time: 9.5367e-07 (backsolve)
Velocity solve time: 1.1493e-01 (backsolve)
Total solve time   : 7.7913e+00
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
