[1;35m
 
Pressure iterations:  45
Velocity iterations:  28 (presolve)      
Velocity iterations: 999 (pressure solve)
Velocity iterations:  19 (backsolve)     
Velocity iterations: 1046 (total solve)   
 
SCR RHS  setup time: 2.5809e-01
SCR RHS  solve time: 6.3547e-01
Pressure setup time: 1.1253e-02
Pressure solve time: 1.6035e+01
Velocity setup time: 7.1526e-07 (backsolve)
Velocity solve time: 1.6295e-01 (backsolve)
Total solve time   : 1.7126e+01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
