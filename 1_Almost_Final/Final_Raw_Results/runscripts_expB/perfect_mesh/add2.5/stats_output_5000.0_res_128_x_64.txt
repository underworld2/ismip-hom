[1;35m
 
Pressure iterations:  38
Velocity iterations:   1 (presolve)      
Velocity iterations:  39 (pressure solve)
Velocity iterations:   1 (backsolve)     
Velocity iterations:  41 (total solve)   
 
SCR RHS  setup time: 1.2438e-01
SCR RHS  solve time: 8.4548e-03
Pressure setup time: 1.6487e-03
Pressure solve time: 3.4904e-01
Velocity setup time: 4.7684e-07 (backsolve)
Velocity solve time: 8.2095e-03 (backsolve)
Total solve time   : 4.9297e-01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
