[1;35m
 
Pressure iterations:  47
Velocity iterations:  36 (presolve)      
Velocity iterations: 1218 (pressure solve)
Velocity iterations:  23 (backsolve)     
Velocity iterations: 1277 (total solve)   
 
SCR RHS  setup time: 2.4911e-01
SCR RHS  solve time: 8.5627e-01
Pressure setup time: 1.5985e-02
Pressure solve time: 2.2031e+01
Velocity setup time: 4.7684e-07 (backsolve)
Velocity solve time: 4.2435e-01 (backsolve)
Total solve time   : 2.3606e+01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
