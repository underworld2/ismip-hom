[1;35m
 
Pressure iterations:  48
Velocity iterations:  21 (presolve)      
Velocity iterations: 726 (pressure solve)
Velocity iterations:  15 (backsolve)     
Velocity iterations: 762 (total solve)   
 
SCR RHS  setup time: 2.4798e-01
SCR RHS  solve time: 6.8919e-01
Pressure setup time: 1.6759e-02
Pressure solve time: 1.2279e+01
Velocity setup time: 4.7684e-07 (backsolve)
Velocity solve time: 2.6723e-01 (backsolve)
Total solve time   : 1.3522e+01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
