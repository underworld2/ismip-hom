[1;35m
 
Pressure iterations:  44
Velocity iterations:  41 (presolve)      
Velocity iterations: 1287 (pressure solve)
Velocity iterations:  26 (backsolve)     
Velocity iterations: 1354 (total solve)   
 
SCR RHS  setup time: 2.4857e-01
SCR RHS  solve time: 4.1258e-01
Pressure setup time: 6.5699e-03
Pressure solve time: 1.2106e+01
Velocity setup time: 7.1526e-07 (backsolve)
Velocity solve time: 2.4302e-01 (backsolve)
Total solve time   : 1.3036e+01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
