[1;35m
 
Pressure iterations:  36
Velocity iterations:   1 (presolve)      
Velocity iterations:  37 (pressure solve)
Velocity iterations:   1 (backsolve)     
Velocity iterations:  39 (total solve)   
 
SCR RHS  setup time: 1.2261e-01
SCR RHS  solve time: 8.2176e-03
Pressure setup time: 1.6398e-03
Pressure solve time: 3.2216e-01
Velocity setup time: 7.1526e-07 (backsolve)
Velocity solve time: 8.3230e-03 (backsolve)
Total solve time   : 4.6418e-01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
