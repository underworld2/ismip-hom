[1;35m
 
Pressure iterations:  65
Velocity iterations:  38 (presolve)      
Velocity iterations: 1097 (pressure solve)
Velocity iterations:  15 (backsolve)     
Velocity iterations: 1150 (total solve)   
 
SCR RHS  setup time: 1.1990e-01
SCR RHS  solve time: 8.1402e-01
Pressure setup time: 1.3813e-02
Pressure solve time: 1.9801e+01
Velocity setup time: 7.1526e-07 (backsolve)
Velocity solve time: 2.6502e-01 (backsolve)
Total solve time   : 2.1060e+01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
