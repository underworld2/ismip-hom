[1;35m
 
Pressure iterations:  49
Velocity iterations:  12 (presolve)      
Velocity iterations: 375 (pressure solve)
Velocity iterations:   9 (backsolve)     
Velocity iterations: 396 (total solve)   
 
SCR RHS  setup time: 3.1602e-03
SCR RHS  solve time: 2.7561e-03
Pressure setup time: 2.9516e-04
Pressure solve time: 5.7434e-02
Velocity setup time: 2.3842e-07 (backsolve)
Velocity solve time: 1.3294e-03 (backsolve)
Total solve time   : 6.5930e-02
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
