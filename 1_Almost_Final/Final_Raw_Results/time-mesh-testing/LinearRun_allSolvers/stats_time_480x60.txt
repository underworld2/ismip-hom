[1;35m
 
Pressure iterations:  59
Velocity iterations:  25 (presolve)      
Velocity iterations: 645 (pressure solve)
Velocity iterations:  14 (backsolve)     
Velocity iterations: 684 (total solve)   
 
SCR RHS  setup time: 9.5346e-02
SCR RHS  solve time: 2.9575e-01
Pressure setup time: 7.7534e-03
Pressure solve time: 6.0060e+00
Velocity setup time: 4.7684e-07 (backsolve)
Velocity solve time: 1.2805e-01 (backsolve)
Total solve time   : 6.5584e+00
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
