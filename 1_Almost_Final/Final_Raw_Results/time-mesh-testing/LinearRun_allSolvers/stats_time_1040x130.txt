[1;35m
 
Pressure iterations:   1
Velocity iterations:  32 (presolve)      
Velocity iterations:   0 (pressure solve)
Velocity iterations:   0 (backsolve)     
Velocity iterations:  32 (total solve)   
 
SCR RHS  setup time: 2.2463e+00
SCR RHS  solve time: 2.1059e+00
Pressure setup time: 3.6380e-02
Pressure solve time: 8.9734e-03
Velocity setup time: 4.7684e-07 (backsolve)
Velocity solve time: 7.1406e-04 (backsolve)
Total solve time   : 4.4967e+00
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
