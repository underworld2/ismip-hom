[1;35m
 
Pressure iterations:  45
Velocity iterations:  32 (presolve)      
Velocity iterations: 640 (pressure solve)
Velocity iterations:  25 (backsolve)     
Velocity iterations: 697 (total solve)   
 
SCR RHS  setup time: 1.3773e+00
SCR RHS  solve time: 1.4829e+00
Pressure setup time: 2.5485e-02
Pressure solve time: 2.5122e+01
Velocity setup time: 7.1526e-07 (backsolve)
Velocity solve time: 1.0003e+00 (backsolve)
Total solve time   : 2.9076e+01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
