[1;35m
 
Pressure iterations:  46
Velocity iterations:  28 (presolve)      
Velocity iterations: 572 (pressure solve)
Velocity iterations:  17 (backsolve)     
Velocity iterations: 617 (total solve)   
 
SCR RHS  setup time: 8.0935e-02
SCR RHS  solve time: 3.1321e-01
Pressure setup time: 5.5943e-03
Pressure solve time: 5.3322e+00
Velocity setup time: 7.1526e-07 (backsolve)
Velocity solve time: 1.5698e-01 (backsolve)
Total solve time   : 5.9108e+00
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
