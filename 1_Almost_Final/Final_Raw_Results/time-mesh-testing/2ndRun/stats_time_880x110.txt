[1;35m
 
Pressure iterations:  45
Velocity iterations:  32 (presolve)      
Velocity iterations: 640 (pressure solve)
Velocity iterations:  25 (backsolve)     
Velocity iterations: 697 (total solve)   
 
SCR RHS  setup time: 1.3792e+00
SCR RHS  solve time: 1.4826e+00
Pressure setup time: 2.5555e-02
Pressure solve time: 2.5079e+01
Velocity setup time: 4.7684e-07 (backsolve)
Velocity solve time: 9.9200e-01 (backsolve)
Total solve time   : 2.9027e+01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
