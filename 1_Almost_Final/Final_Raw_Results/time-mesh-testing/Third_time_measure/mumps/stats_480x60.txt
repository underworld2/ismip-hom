[1;35m
 
Pressure iterations:  44
Velocity iterations:   1 (presolve)      
Velocity iterations:  45 (pressure solve)
Velocity iterations:   1 (backsolve)     
Velocity iterations:  47 (total solve)   
 
SCR RHS  setup time: 5.4427e-01
SCR RHS  solve time: 3.0197e-02
Pressure setup time: 5.5690e-03
Pressure solve time: 1.4391e+00
Velocity setup time: 7.1526e-07 (backsolve)
Velocity solve time: 3.0143e-02 (backsolve)
Total solve time   : 2.0557e+00
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
