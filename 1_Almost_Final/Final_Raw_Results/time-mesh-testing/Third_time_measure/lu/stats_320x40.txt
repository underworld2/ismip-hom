[1;35m
 
Pressure iterations:  45
Velocity iterations:   1 (presolve)      
Velocity iterations:  46 (pressure solve)
Velocity iterations:   1 (backsolve)     
Velocity iterations:  48 (total solve)   
 
SCR RHS  setup time: 3.2502e-01
SCR RHS  solve time: 3.6831e-03
Pressure setup time: 2.5318e-03
Pressure solve time: 2.0524e-01
Velocity setup time: 2.3842e-07 (backsolve)
Velocity solve time: 3.6530e-03 (backsolve)
Total solve time   : 5.4161e-01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
