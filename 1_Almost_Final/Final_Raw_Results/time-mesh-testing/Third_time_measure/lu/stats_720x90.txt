[1;35m
 
Pressure iterations:  41
Velocity iterations:   1 (presolve)      
Velocity iterations:  42 (pressure solve)
Velocity iterations:   1 (backsolve)     
Velocity iterations:  44 (total solve)   
 
SCR RHS  setup time: 3.6131e+00
SCR RHS  solve time: 2.4881e-02
Pressure setup time: 1.6108e-02
Pressure solve time: 1.2443e+00
Velocity setup time: 4.7684e-07 (backsolve)
Velocity solve time: 2.4854e-02 (backsolve)
Total solve time   : 4.9419e+00
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
