[1;35m
 
Pressure iterations:  41
Velocity iterations:   1 (presolve)      
Velocity iterations:  42 (pressure solve)
Velocity iterations:   1 (backsolve)     
Velocity iterations:  44 (total solve)   
 
SCR RHS  setup time: 6.3270e-01
SCR RHS  solve time: 6.2687e-03
Pressure setup time: 3.9189e-03
Pressure solve time: 3.1881e-01
Velocity setup time: 2.3842e-07 (backsolve)
Velocity solve time: 6.2046e-03 (backsolve)
Total solve time   : 9.7245e-01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
