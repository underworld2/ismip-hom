[1;35m
 
Pressure iterations:  40
Velocity iterations:   1 (presolve)      
Velocity iterations:  41 (pressure solve)
Velocity iterations:   1 (backsolve)     
Velocity iterations:  43 (total solve)   
 
SCR RHS  setup time: 6.5396e+00
SCR RHS  solve time: 3.9857e-02
Pressure setup time: 2.4032e-02
Pressure solve time: 1.9278e+00
Velocity setup time: 2.3842e-07 (backsolve)
Velocity solve time: 3.9719e-02 (backsolve)
Total solve time   : 8.5994e+00
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
