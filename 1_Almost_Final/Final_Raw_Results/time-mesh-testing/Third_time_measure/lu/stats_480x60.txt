[1;35m
 
Pressure iterations:  44
Velocity iterations:   1 (presolve)      
Velocity iterations:  45 (pressure solve)
Velocity iterations:   1 (backsolve)     
Velocity iterations:  47 (total solve)   
 
SCR RHS  setup time: 1.0772e+00
SCR RHS  solve time: 9.6526e-03
Pressure setup time: 5.5923e-03
Pressure solve time: 5.1999e-01
Velocity setup time: 4.7684e-07 (backsolve)
Velocity solve time: 9.6016e-03 (backsolve)
Total solve time   : 1.6283e+00
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
