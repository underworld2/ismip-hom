#!/bin/bash

HOME=$PWD

for s in mumps lu mg
do

    mkdir $s
    cp *.py $s/.
    cd $s

    for f in *.py
    do
        echo $f&&
        docker run -v $PWD:/home/jovyan/workspace -w /home/jovyan/workspace --rm -it underworldcode/underworld2:latest taskset -c 1 python $f $s &&
        echo 'next'
    done
    cd $HOME
done
