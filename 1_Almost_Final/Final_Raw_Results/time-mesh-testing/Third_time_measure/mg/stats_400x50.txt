[1;35m
 
Pressure iterations:  45
Velocity iterations:  22 (presolve)      
Velocity iterations: 477 (pressure solve)
Velocity iterations:  11 (backsolve)     
Velocity iterations: 510 (total solve)   
 
SCR RHS  setup time: 1.4903e-01
SCR RHS  solve time: 1.5988e-01
Pressure setup time: 3.9155e-03
Pressure solve time: 2.9223e+00
Velocity setup time: 4.7684e-07 (backsolve)
Velocity solve time: 6.6001e-02 (backsolve)
Total solve time   : 3.3132e+00
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
