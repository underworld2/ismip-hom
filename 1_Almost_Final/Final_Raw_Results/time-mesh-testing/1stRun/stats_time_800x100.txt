[1;35m
 
Pressure iterations:  90
Velocity iterations: 187 (presolve)      
Velocity iterations: 1572 (pressure solve)
Velocity iterations:  90 (backsolve)     
Velocity iterations: 1849 (total solve)   
 
SCR RHS  setup time: 2.8212e-01
SCR RHS  solve time: 5.9581e+00
Pressure setup time: 2.1630e-02
Pressure solve time: 4.7242e+01
Velocity setup time: 7.1526e-07 (backsolve)
Velocity solve time: 2.7829e+00 (backsolve)
Total solve time   : 5.6354e+01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
