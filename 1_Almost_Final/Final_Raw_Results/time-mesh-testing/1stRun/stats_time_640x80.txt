[1;35m
 
Pressure iterations:  49
Velocity iterations:  40 (presolve)      
Velocity iterations: 904 (pressure solve)
Velocity iterations:  28 (backsolve)     
Velocity iterations: 972 (total solve)   
 
SCR RHS  setup time: 1.0271e-01
SCR RHS  solve time: 8.2893e-01
Pressure setup time: 9.8829e-03
Pressure solve time: 1.6442e+01
Velocity setup time: 7.1526e-07 (backsolve)
Velocity solve time: 5.1835e-01 (backsolve)
Total solve time   : 1.7943e+01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
