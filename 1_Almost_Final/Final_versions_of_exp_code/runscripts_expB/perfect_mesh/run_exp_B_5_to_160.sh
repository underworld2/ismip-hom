#!/bin/bash

HOME=$PWD

echo $HOME

for f in {0..5}
do

  echo $f&&
  
  docker run -v $PWD:/home/jovyan/workspace -w /home/jovyan/workspace --rm -it underworldcode/underworld2:latest python Experiment_B.py $f &&
  
  echo 'next'
  cd $HOME
  
done
