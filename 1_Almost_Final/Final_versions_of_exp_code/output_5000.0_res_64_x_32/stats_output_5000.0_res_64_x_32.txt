[1;35m
 
Pressure iterations:  53
Velocity iterations:  82 (presolve)      
Velocity iterations: 6232 (pressure solve)
Velocity iterations:  58 (backsolve)     
Velocity iterations: 6372 (total solve)   
 
SCR RHS  setup time: 5.8441e-03
SCR RHS  solve time: 5.9283e-02
Pressure setup time: 4.9686e-04
Pressure solve time: 3.9218e+00
Velocity setup time: 7.1526e-07 (backsolve)
Velocity solve time: 3.5466e-02 (backsolve)
Total solve time   : 4.0265e+00
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
