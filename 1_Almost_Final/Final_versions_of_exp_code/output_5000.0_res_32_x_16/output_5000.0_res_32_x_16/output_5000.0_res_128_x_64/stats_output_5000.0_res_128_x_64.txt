[1;35m
 
Pressure iterations:  51
Velocity iterations:  18 (presolve)      
Velocity iterations: 548 (pressure solve)
Velocity iterations:  16 (backsolve)     
Velocity iterations: 582 (total solve)   
 
SCR RHS  setup time: 1.8505e-02
SCR RHS  solve time: 7.7366e-02
Pressure setup time: 1.9019e-03
Pressure solve time: 1.7326e+00
Velocity setup time: 7.1526e-07 (backsolve)
Velocity solve time: 4.9816e-02 (backsolve)
Total solve time   : 1.8925e+00
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
