[1;35m
 
Pressure iterations:  43
Velocity iterations:  20 (presolve)      
Velocity iterations: 469 (pressure solve)
Velocity iterations:  17 (backsolve)     
Velocity iterations: 506 (total solve)   
 
SCR RHS  setup time: 3.0670e-01
SCR RHS  solve time: 3.8778e-01
Pressure setup time: 8.3869e-03
Pressure solve time: 7.4223e+00
Velocity setup time: 9.5367e-07 (backsolve)
Velocity solve time: 2.7820e-01 (backsolve)
Total solve time   : 8.4335e+00
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
