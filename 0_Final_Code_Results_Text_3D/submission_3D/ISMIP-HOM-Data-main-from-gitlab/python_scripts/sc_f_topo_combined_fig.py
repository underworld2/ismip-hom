#!/bin/env python3

##
from pylab import *
import glob
import os
from scipy import signal
c = os.getcwd()

##

dirs = glob.glob('expf*')
print(dirs)
fig, ax = subplots(1, constrained_layout=True, figsize=(10,10))

place =	{
  "000": (0),
  #"001": (1),
}

colors = {
    "cma1": "LightSeaGreen",
    "oga1": "LimeGreen"
    #"tsa1": "gold"
}

legflag = True

for d in dirs:
    
    os.chdir(d)
    print(d)

    files = glob.glob ('*.csv')
    
    # make namelist
    namelist = []
    for i in files:
        namelist.append(i[:4])
        
    name = files[0][4:8] + ".svg"
    pos = place[name[1:4]]
    
    legflag = True if name[1:4] == '000' else False
    
    # get index of own simulation
    ind = namelist.index('jla1')

    # move entry to back of list
    namelist.append(namelist.pop(ind)) 
    files.append(files.pop(ind))

    # load data
    data_list = []
    for i in files:
        data_list.append(loadtxt(i))
    
    for i, j, k in zip(data_list, files, namelist):
        if k == 'jla1':
            xplot_list = []
            yplot_list = []
            for m in range(0,len(i)):
                #if(np.logical_and(i[:,1][m] > 0.24, i[:,1][m] < 0.26).all()): 
                xplot_list.append(i[:,0][m])
                yplot_list.append(i[:,1][m]) 
                #i[:,1][m]=signal.savgol_filter(i[:,1][m], len(i[:,1]), 3),   
            ax.plot(xplot_list, yplot_list, '-', color = "black", label = "this\nstudy" if legflag else "", linewidth=4.)
            #print(np.min(i[:,2]))
            print(np.max(i[:,1]))
            a = np.where(i[:,1]==np.max(i[:,1]))
            print( i[:,0][a])
        else:
            xplot_list = []
            yplot_list = []
            for m in range(0,len(i)):
                if(np.logical_and(i[:,1][m] > -1.0, i[:,1][m] < 1.0).all()): 
                    xplot_list.append(i[:,0][m])
                    yplot_list.append(i[:,2][m]) 
            ax.plot(xplot_list, yplot_list, color = colors[k], label = j[0:4] if legflag else "", linewidth=1.)
            #print(np.min(i[:,7]))
            #read maximum x and y
            print(np.max(i[:,2]))
            a = np.where(i[:,2]==np.max(i[:,2]))
            print( i[:,0][a])
    
    if legflag:
        ax.legend(ncol=2)

    ax.set_ylabel('surface height(m)')
    ax.set_xlabel('distance from center (km)')

    t = j[5:8].lstrip('0') 

    ax.set_title(t)
    
    os.chdir(c)

#show()
#fig.legend( ncol=4 )
fig.savefig('fig_expf_topo_all.svg')
fig.savefig('fig_expf_topo_all.png')
fig.savefig('fig_expf_topo_all.pdf')
