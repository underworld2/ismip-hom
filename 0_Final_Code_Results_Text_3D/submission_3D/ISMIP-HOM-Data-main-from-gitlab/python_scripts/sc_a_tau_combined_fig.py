#!/bin/env python3

##
from pylab import *
import glob
import os

c = os.getcwd()

##

dirs = glob.glob('expa*')
print(dirs)
fig, ax = subplots(3, 2, constrained_layout=True, figsize=(10,10))

place =	{
  "005": (0,0),
  "010": (0,1),
  "020": (1,0),
  "040": (1,1),
  "080": (2,0),
  "160": (2,1),
}

colors = {
    "aas2": "MediumTurquoise",
    "cma1": "LightSeaGreen",
    "jvj1": "red",
    "mmr1": "blue",
    "oga1": "LimeGreen",
    "rhi1": "DarkKhaki",
    "rhi3": "magenta",
    "ssu1": "silver",
    "spr1": "Chocolate"
}

legflag = True

for d in dirs:
    
    os.chdir(d)
    print(d)

    files = glob.glob ('*.csv')
    
    # make namelist
    namelist = []
    for i in files:
        namelist.append(i[:4])
        
    name = files[0][4:8] + ".svg"
    pos = place[name[1:4]]
    
    legflag = True if name[1:4] == '160' else False
    
    # get index of own simulation
    ind = namelist.index('jla1')

    # move entry to back of list
    namelist.append(namelist.pop(ind)) 
    files.append(files.pop(ind))

    #print (namelist)
    #print (files)

    # load data
    data_list = []
    #print(i)
    for i in files:
        data_list.append(loadtxt(i))
    
    for i, j, k in zip(data_list, files, namelist):
        if k == 'jla1':
            xplot_list = []
            yplot_list = []
            for m in range(0,len(i)):
                if(np.logical_and(i[:,1][m] > 0.235, i[:,1][m] < 0.258).all()): 
                    xplot_list.append(i[:,0][m])
                    yplot_list.append(i[:,5][m]) 
            ax[pos].plot(xplot_list, yplot_list, '-', color = "black", label = "this\nstudy" if legflag else "", linewidth=4.)
        else:
            xplot_list = []
            yplot_list = []
            for m in range(0,len(i)):
                if(np.logical_and(i[:,1][m] > 0.24, i[:,1][m] < 0.26).all()): 
                    xplot_list.append(i[:,0][m])
                    yplot_list.append(i[:,5][m]) 
            ax[pos].plot(xplot_list, yplot_list, color = colors[k], label = j[0:4] if legflag else "", linewidth=1.)
    if legflag:
        ax[pos].legend(ncol=2)

    ax[pos].set_ylabel('shear stress (kPa)')
    ax[pos].set_xlabel('x norm.')

    t = files[0][5:8].lstrip('0') + ' km'

    ax[pos].set_title(t)
    
    os.chdir(c)

#show()
#fig.legend( ncol=4 )
fig.savefig('fig_expa_tau_all.svg')
fig.savefig('fig_expa_tau_all.png')
fig.savefig('fig_expa_tau_all.pdf')
