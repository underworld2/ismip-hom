#!/bin/env python3

##
from pylab import *
import glob
import os

c = os.getcwd()

##

dirs = glob.glob('expb*')
print(dirs)
fig, ax = subplots(3, 2, constrained_layout=True, figsize=(10,10))

place =	{
  "005": (0,0),
  "010": (0,1),
  "020": (1,0),
  "040": (1,1),
  "080": (2,0),
  "160": (2,1),
}

colors = {
    "tsa1": "black",
    "tsa2": "red",
    "jla1": "red",
}

legflag = True

for d in dirs:
    
    os.chdir(d)
    print(d)

    files = glob.glob ('*.csv')
    
    # make namelist
    namelist = []
    for i in files:
        namelist.append(i[:4])
        
    name = files[0][4:8] + ".svg"
    pos = place[name[1:4]]
    
    # load data
    data_list = []
    for i in files:
        data_list.append(loadtxt(i))

    for i, j, k in zip(data_list, files, namelist):
        ax[pos].plot(i[:,0], i[:,1], '-', color = colors[k], label = "tsa1 (2D)" if (j[0:4] == "tsa1") else (j[0:4] + " (3D)"), linewidth=2.)
    
    ax[pos].legend(ncol=2)

    ax[pos].set_ylabel('velovity (m/a)')
    ax[pos].set_xlabel('x norm.')

    t = j[5:8].lstrip('0') + ' km'

    ax[pos].set_title(t)
    
    os.chdir(c)

#show()
#fig.legend( ncol=4 )
fig.savefig('fig_expb_v_all.svg')
fig.savefig('fig_expb_v_all.png')
fig.savefig('fig_expb_v_all.pdf')
