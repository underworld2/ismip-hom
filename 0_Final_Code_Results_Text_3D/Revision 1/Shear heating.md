With $\dot E$ the rate of energy dissipation per unit volume (1 $m³$) we have the following relations: 

(1) $\dot E = \sigma_s \dot \epsilon$

(2) $\dot E = c \frac {\partial T}{\partial t}$

Eq (2) is for a constant stress / strain rate and no conduction.



From (2) we get: (3) $\Delta T = \dot E * \Delta t / c$



With $\dot \epsilon = 2A\sigma_s^n$ we get $\sigma_s = \sqrt[n]{\frac{\dot \epsilon}{2A}}$

From (1): $\dot E = \dot \epsilon ^{1 + 1/n} / \sqrt{2A}$

Aith A = 1e-16 1/Pa^n 1/a , $\dot \epsilon$ = 0.005 1/a , c = 2108 and density 917 kg/m³ we will have:

$\Delta T = \frac {0.005**{1+1/3}}{\sqrt(2 \cdot 10^{-16})} \cdot \frac {1}{2108 \cdot 917} =$ 0.031 K/a

