[1;35m
 
Pressure iterations:  49
Velocity iterations:  47 (presolve)      
Velocity iterations: 1403 (pressure solve)
Velocity iterations:  25 (backsolve)     
Velocity iterations: 1475 (total solve)   
 
SCR RHS  setup time: 1.7397e-02
SCR RHS  solve time: 1.0569e-01
Pressure setup time: 1.6704e-03
Pressure solve time: 2.8752e+00
Velocity setup time: 4.7684e-07 (backsolve)
Velocity solve time: 5.0676e-02 (backsolve)
Total solve time   : 3.0586e+00
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
