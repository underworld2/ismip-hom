#!/bin/bash

for f in *.py
do
  echo $f&&
  docker run -v $PWD:/home/jovyan/workspace -w /home/jovyan/workspace --rm -it underworldcode/underworld2:latest taskset -c 1 python $f &&
  echo 'next'
done
