[1;35m
 
Pressure iterations:  29
Velocity iterations:  58 (presolve)      
Velocity iterations: 1004 (pressure solve)
Velocity iterations:  38 (backsolve)     
Velocity iterations: 1100 (total solve)   
 
SCR RHS  setup time: 1.7498e-02
SCR RHS  solve time: 1.3663e-01
Pressure setup time: 2.3603e-03
Pressure solve time: 2.0605e+00
Velocity setup time: 4.7684e-07 (backsolve)
Velocity solve time: 7.8041e-02 (backsolve)
Total solve time   : 2.3039e+00
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
