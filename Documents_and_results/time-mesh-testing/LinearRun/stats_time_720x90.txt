[1;35m
 
Pressure iterations:  47
Velocity iterations:  27 (presolve)      
Velocity iterations: 610 (pressure solve)
Velocity iterations:  18 (backsolve)     
Velocity iterations: 655 (total solve)   
 
SCR RHS  setup time: 8.0213e-01
SCR RHS  solve time: 8.1152e-01
Pressure setup time: 1.7200e-02
Pressure solve time: 1.5118e+01
Velocity setup time: 7.1526e-07 (backsolve)
Velocity solve time: 4.4417e-01 (backsolve)
Total solve time   : 1.7243e+01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
