[1;35m
 
Pressure iterations:  52
Velocity iterations:  54 (presolve)      
Velocity iterations: 925 (pressure solve)
Velocity iterations:  32 (backsolve)     
Velocity iterations: 1011 (total solve)   
 
SCR RHS  setup time: 2.9923e-01
SCR RHS  solve time: 1.8348e+00
Pressure setup time: 2.1356e-02
Pressure solve time: 2.7840e+01
Velocity setup time: 4.7684e-07 (backsolve)
Velocity solve time: 9.8263e-01 (backsolve)
Total solve time   : 3.1050e+01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
