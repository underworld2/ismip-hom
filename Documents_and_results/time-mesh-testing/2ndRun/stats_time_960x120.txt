[1;35m
 
Pressure iterations:  44
Velocity iterations:  47 (presolve)      
Velocity iterations: 674 (pressure solve)
Velocity iterations:  31 (backsolve)     
Velocity iterations: 752 (total solve)   
 
SCR RHS  setup time: 2.5694e-01
SCR RHS  solve time: 2.4099e+00
Pressure setup time: 3.1152e-02
Pressure solve time: 2.9933e+01
Velocity setup time: 7.1526e-07 (backsolve)
Velocity solve time: 1.4151e+00 (backsolve)
Total solve time   : 3.4146e+01
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
