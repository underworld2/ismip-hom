[1;35m
 
Pressure iterations:  43
Velocity iterations:  26 (presolve)      
Velocity iterations: 529 (pressure solve)
Velocity iterations:  13 (backsolve)     
Velocity iterations: 568 (total solve)   
 
SCR RHS  setup time: 3.9217e-01
SCR RHS  solve time: 4.5989e-01
Pressure setup time: 1.0410e-02
Pressure solve time: 7.6613e+00
Velocity setup time: 4.7684e-07 (backsolve)
Velocity solve time: 1.8416e-01 (backsolve)
Total solve time   : 8.7364e+00
 
Velocity solution min/max: 0.0000e+00/0.0000e+00
Pressure solution min/max: 0.0000e+00/0.0000e+00
 
[00m
