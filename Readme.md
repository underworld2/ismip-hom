This repo contains python-scripts used to benchmark the finite-element software 'Underworld2' for glacier flow.

## Note
All files for the related article are stored here, including a lot of non-source code. The relevant final code + results can be fonud under **0_Final_Code_Results_Text**.

## Experiments

Experiments are based on Pattyn et al., the standard for such models. These experiments consider powerlaw isotropic ice with n=3 under 2D and 3D conditions.

In addition we apply own experiments, where we evaluate the anisotropic capabilities of 'Underworld2'.

![Compare isotropic flow with anisotropic flow.](Aniso-vs-Iso-flow.png)

*The image compares deformation of flowing ice which can either be (b) isotropic or (c) anisotropic. Marker lines prior to (a) and after 750 years of flow. Green: bedrock, flow to the right.*

### Relevant publication

Sachau, Till, Haibin Yang, Justin Lang, Paul D. Bons and Louis Moresi. „ISMIP-HOM Benchmark Experiments Using Underworld“. Geoscientific Model Development 15, Nr. 23 (2. Dezember 2022): 8749–64. https://doi.org/10.5194/gmd-15-8749-2022.
