#!/bin/env python3

import os
from pylab import *
import glob

cwd = os.getcwd()

##

dirs = glob.glob ('output_*.0')

for d in dirs:
    os.chdir(cwd)
    os.chdir(d)
    files = glob.glob ('output_*.csv')
    file = files[0]
    num = file[7:12]
    num = num.replace(".", "0")
    num = int(int(num)/1000)
    num =str(num)
    if num == "16":
        num += "0"
    elif num == "50":
        num = num[:-1]
        num = "00" + num
    else:
        num = "0" + num
    print (num)

    data = loadtxt(file,skiprows=1,delimiter=',')

    #print (data)

    #for i in len(data[0]):
    data2 = zeros((len(data), 6))
    data2[:,0] = data[:,0] / data[-1,0]
    data2[:,1] = sqrt (data[:,2]**2 + data[:,3]**2)
    data2[:,2] = data[:,3]
    data2[:,3] = data[:,4]
    data2[:,4] = data[:,5] / 1000.
    # isotropic pressure (from the model
    # - hydrostatic
    # correction is for 1 element of height 2000/128
    data2[:,5] = (data[:,7] - 1000. * 910 * 9.81) / 1000

    savetxt('tsa1d' + num + '.csv', data2, delimiter=' ')
    os.system('cp tsa1d' + num + '.csv' + ' ../.')  
