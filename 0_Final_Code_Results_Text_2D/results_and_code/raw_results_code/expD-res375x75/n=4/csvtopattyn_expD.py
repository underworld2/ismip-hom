#!/bin/env python3

import os
from pylab import *
import glob

print(os.getcwd())

##

files = glob.glob ('output_*.csv')
file = files[0]

data = loadtxt(file,skiprows=1,delimiter=',')

#print (data)

#for i in len(data[0]):
data2 = zeros((len(data), 6))
data2[:,0] = data[:,0] / data[-1,0]
data2[:,1] = data[:,2]
data2[:,2] = data[:,3]
data2[:,3] = data[:,4]
data2[:,4] = data[:,5] / 1000.
# isotropic pressure (from the model
# - hydrostatic
# correction is for 1 element of height 2000/128
data2[:,5] = (data[:,7] - 1000. * 910 * 9.81) / 1000

savetxt('tsa1d.csv', data2, delimiter=' ')
