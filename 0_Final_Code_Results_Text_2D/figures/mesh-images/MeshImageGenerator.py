#!/usr/bin/env python
# coding: utf-8

# # Basic python imports and model settings

# In[41]:


import underworld.visualisation as vis

from underworld import function as fn
import underworld as uw

import matplotlib.pyplot as pyplot
import numpy as np
from scipy.spatial import distance

import math
import os
import sys

import time

from scipy.signal import savgol_filter

# deform the mesh geometry towards the ice-rock interface?
deform_mesh = False
perfect_mesh = False

maxX = 5. * 1000.
min_bed_height = 500.           # we want a minimum of 500 m of rock beneath the ice
omega = 2 * np.pi / maxX
amplitude = 500.
average_bedthickness = 1000.
surface_height = average_bedthickness + amplitude + min_bed_height
maxY = surface_height

minY = minX = 0.

g = 9.81
ice_density = 910.

A = 1e-16
n = 3.

resX = 30
resY = 10

print("resX: " + str(resX) + " resY: " + str(resY))

delta_timestep = 1.						# in years, used in the main loop
# after how many timesteps do we need new figures?
update_figures_after_n_timesteps = 1
number_of_deformation_lines = 11

number_of_deformation_points = 50000
distance_between_deformation_lines = maxY / (number_of_deformation_lines + 1)

cell_height = maxY / resY
cell_width = maxX / resX


# # Mesh + mesh variables

# In[42]:

elementType = "Q1/dQ0"
#elementType = "Q2/dQ1"
#elementType = "Q1/dPc1"
#elementType = "Q2/dPc1"

mesh = uw.mesh.FeMesh_Cartesian(elementType=(elementType),
                                elementRes=(resX, resY),
                                minCoord=(minX, minY),
                                maxCoord=(maxX, maxY),
                                periodic=[True, False])

submesh = mesh.subMesh

# save the mesh
# mesh.save(outputPath + "mesh.h5")

velocityField = uw.mesh.MeshVariable(mesh=mesh, nodeDofCount=mesh.dim)
#pressureField = uw.mesh.MeshVariable(mesh=mesh.subMesh, nodeDofCount=1)
pressureField = uw.mesh.MeshVariable(mesh=mesh, nodeDofCount=1)

viscosityField = uw.mesh.MeshVariable(mesh=mesh, nodeDofCount=1)

strainRateField = mesh.add_variable(nodeDofCount=1)

#pressureField.data[:] = 0.
velocityField.data[:] = [0., 0.]

# Initialise the 'materialVariable' data to represent different materials.
materialA = 0  	# accommodation layer, a.k.a. Sticky Air
materialV = 1  	# ice, isotropic
materialR = 2   # rock
materialT = 5   # test material in order to test eg interface detection

coord = fn.input()

z_bed_function = surface_height - average_bedthickness + amplitude * fn.math.sin(omega * coord[0])

## Define the ice-rock / ice-air interface 
#  In[43]:

botSet = mesh.specialSets['Bottom_VertexSet']
topSet = mesh.specialSets['Top_VertexSet']

dx = (maxX - minX) / resX
iceRockInterface = []
iceAirInterface = []

dx = (maxX - minX) / resX

for index in range(resX+1):
    
    start_x = dx * index

    interface_y =  surface_height - average_bedthickness + amplitude * np.sin(start_x * (np.pi * 2.) / (maxX-minX) )
                
    #ind = np.where(abs(mesh.data[:,0] - start_x) < 0.01*dx)
    iceRockInterface.append([start_x, interface_y])
    iceAirInterface.append([start_x, maxY])


# # Deform mesh to be almost structurally conforming

# In[15]:


def mesh_deform(section,fixPoint,mi):
    
    # fixPoint: the position to be refined
    # mi: representing the gradient of mesh resolution; the larger mi, the larger gradient
    # generally, mi ranges from 0.1 to 3, dependnig on the resolution 
 
    # note from Till: 
    # I normalize it, because then it becomes more clear to me.
    # normalization is not strictly necessary
    normalize = maxY - minY

    section /= normalize
    fixPoint /= normalize

    for index in range(len(section)):
        
        maxCoord = np.max(section)
        minCoord = np.min(section)
        
        if  section[index] < fixPoint:
            zz_sqrt = (fixPoint - section[index])**mi
            zz_sqrt_max = (fixPoint - minCoord)**mi
            section[index] = fixPoint - (fixPoint - section[index]) * zz_sqrt / zz_sqrt_max

        if  section[index] > fixPoint:
            zz_sqrt = (section[index] - fixPoint)**mi
            zz_sqrt_max = (maxCoord - fixPoint)**mi
            section[index] = fixPoint + (section[index] - fixPoint) * zz_sqrt/zz_sqrt_max        
    
    return (section * normalize)

dx = (maxX - minX) / resX

if deform_mesh:
    
    with mesh.deform_mesh():

        for index in range(resX+1):  

            start_x = dx * index

            interface_y =  surface_height - average_bedthickness + amplitude * np.sin(start_x * (np.pi * 2.) / (maxX-minX) )

            #print (start_x,interface_y)
            #lx.append(start_x)
            #ly.append(interface_y)

            #ind = np.where(abs(mesh.data[:,0] - start_x) < 0.01*dx)
            ind = np.where(abs(mesh.data[:,0] - start_x) < 0.01*dx)

            mesh.data[ind[0],1] = mesh_deform(mesh.data[ind[0],1], interface_y, 0.75)

def mesh_deform_Ind(section,fixPoint_index,fixPoint,mi):
    
    section[int(fixPoint_index)] = fixPoint
    seqN = len(section)
    
    # fixPoint_index (int): specify the index of the section to be at the place need to be refined
    # fixPoint: the position to be refined
    # mi: representing the gradient of mesh resolution; the larger mi, the larger gradient
    for index in range(len(section)):
        
        maxCoord = np.max(section)
        minCoord = np.min(section)
        
        if  index < fixPoint_index:
            section[index] = minCoord + index*(fixPoint-minCoord)/fixPoint_index            
            zz_sqrt = (fixPoint-section[index])**mi
            zz_sqrt_max = (fixPoint-minCoord)**mi
            section[index] = fixPoint-(fixPoint-section[index])*zz_sqrt/zz_sqrt_max

        if  index > fixPoint_index:
            section[index] = fixPoint + (index-fixPoint_index)*(maxCoord-fixPoint)/(seqN-fixPoint_index-1)
            zz_sqrt = (section[index]-fixPoint)**mi
            zz_sqrt_max = (maxCoord-fixPoint)**mi
            section[index] =fixPoint+(section[index]-fixPoint)*zz_sqrt/zz_sqrt_max    
            
    return (section)

#picswarm = uw.swarm.VoronoiIntegrationSwarm(swarm)
#picswarm.repopulate()

# visualise the result

if (perfect_mesh):
    
    with mesh.deform_mesh():
        
        for index in range(resX+1):  
            start_x = dx * index
            
            interface_y =  surface_height - average_bedthickness + amplitude * np.sin(start_x * (np.pi * 2.) / (maxX-minX) )
            
            ind = np.where(abs(mesh.data[:,0]-start_x)<0.01*dx)
            
            mesh.data[ind[0],1] = mesh_deform_Ind(mesh.data[ind[0],1],resY/2,interface_y,0.5)
    # make the swarm belong the element that is the same as the in the regular mesh
 
swarm = uw.swarm.Swarm(mesh=mesh, particleEscape=True)

part_per_cell = 20000

swarmLayout = uw.swarm.layouts.PerCellSpaceFillerLayout(swarm=swarm, particlesPerCell=part_per_cell)
#swarmLayout = uw.swarm.layouts.PerCellGaussLayout( swarm=swarm, gaussPointCount=5 )
swarm.populate_using_layout(layout=swarmLayout)

materialVariable = swarm.add_variable(dataType="int", count=1)

particleDensity = swarm.add_variable(dataType="double", count=1)
particleDensity.data[:] = 0.0

coord = fn.input()

conditions = [(coord[1] > surface_height, materialA),
              (coord[1] < z_bed_function, materialR),
              (True, materialV)]

materialVariable.data[:] = fn.branching.conditional(conditions).evaluate(swarm)

#pyplot.plot(measurementSwarm.data[:,0], measurementSwarm.data[:,1], color='black')

figMesh = vis.Figure(figsize=(1200,600))
figMesh.append( vis.objects.Points( swarm, materialVariable, pointSize=1.0 ) )
figMesh.append( vis.objects.Mesh(mesh) )
figMesh.save_image("mesh_deform.png")

