#!/bin/env python3

##
from pylab import *
import glob

import os

#print(os.getcwd())

##
files = glob.glob ('*.csv')

# make namelist
namelist = []
for i in files:
    namelist.append(i[:4])
    
name = files[0][4:8] + ".svg"
print(name)

# get index of own simulation
ind = namelist.index('tsa1')

# move entry to back of list
namelist.append(namelist.pop(ind)) 
files.append(files.pop(ind))

#print (namelist)
#print (files)

# load data
data_list = []
for i in files:
    data_list.append(loadtxt(i))

    
for i, j in zip(data_list, namelist):
    if 'tsa1' in j:
        #print ('dingdong')
        plot(i[:,0], i[:,3], 'r-', label = 'tsa1', linewidth=3.)
    else:
        plot(i[:,0], i[:,3], 'k-', linewidth=1.)
    
legend()

ylabel('kPa')
xlabel('x norm.')

t = files[0][5:8].lstrip('0') + ' km'

title(t)

#show()
savefig(name)
