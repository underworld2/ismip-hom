#!/bin/env python3

##
from pylab import *
import glob
import os

c = os.getcwd()

##

dirs = glob.glob('exp*')

fig, ax = subplots(1, 2, constrained_layout=True, figsize=(10,5))

place =	{
  "005": (0,0),
  "010": (0,1),
  "020": (1,0),
  "040": (1,1),
  "080": (2,0),
  "160": (2,1),
}

colors = {
    "aas2": "Orange",
    "cma1": "LightSeaGreen",
    "jvj1": "Red",
    "mmr1": "Blue",
    "oga1": "LimeGreen",
    "rhi1": "DarkKhaki",
    "rhi3": "Magenta",
    "ssu1": "Silver",
    "spr1": "Chocolate"
}

files = glob.glob (dirs[0] + '/*.csv')

csvfilenames = []
for f in files:
    csvfilenames.append(os.path.basename(f))

print (csvfilenames)

# get index of own simulation
matching = [s for s in csvfilenames if "tsa1" in s]
ind = csvfilenames.index(matching[0])
# move entry to back of list
csvfilenames.append(csvfilenames.pop(ind)) 

for c in csvfilenames:
    
    name = c[0:4]

    filenamelist = []
    x = []
    for d in dirs:
        f = glob.glob(d + "/" + name + "*.csv")[0]
        filenamelist.append(f)
        x.append(int(f[-7:-4]))

    # load data
    data_list = []
    for i in filenamelist:
        data_list.append(loadtxt(i))
    
    minlist = []
    maxlist = []
    for i in data_list:
        maxlist.append(max(i[:,3]))
        minlist.append(min(i[:,3]))
        
    # sort lists
    x, maxlist, minlist = (list(t) for t in zip(*sorted(zip(x, maxlist, minlist))))
    
    print (minlist)
    
    if name == 'tsa1':
        ax[0].plot(x, maxlist, '-o', color = "black", label = "this study", linewidth=2.)
    else:
        ax[0].plot(x, maxlist, color = colors[name], label = name , linewidth=1.)

    ax[0].set_ylabel('v (m/a)')
    ax[0].set_xlabel('model size')

    ax[0].set_title("Max. basal velocities")
    ax[0].legend(ncol = 2)

    ax[0].set_xticks(x)
        
    if name == 'tsa1':
        ax[1].plot(x, minlist, '-o', color = "black", linewidth=2.)
    else:
        ax[1].plot(x, minlist, color = colors[name], linewidth=1.)
    
    ax[1].set_ylabel('v (m/a)')
    ax[1].set_xlabel('model size')

    ax[1].set_title("Min. basal velocities")
    ax[1].legend(ncol = 2)

    ax[1].set_xticks(x)

savefig('fig_expd_vbminmax_n4_compared.svg')

