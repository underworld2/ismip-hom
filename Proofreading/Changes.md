# Corrections to the proof

## Own remarks in addition to the remarks by the copy-editor and the typesetter

- Page 15, line 15-16: 
    please change http-link 
    from
    http://homepages.ulb.ac.be/~fpattyn/ismip/
    to:
    https://frank.pattyn.web.ulb.be/ismip/welcome.html

- Page 13, line:
    please change from:
    Gillet-Chaulet et al. (2005)
    to:
    Gillet-Chaulet et al. (2006)

## Remarks from the language copy-editor

- CE1:Please note that the use of quotation marks is not necessary in addition to the letter labels for identifying the experiments. The quotation marks have been removed throughout.

    ok

- CE2 Underworld has already been mentioned several times; therefore the quotation marks have been deleted.

    ok

- CE3 Please confirm that you did mean “along” here.
  
    better: _across_

- CE4 Please confirm the hyphen. It conveys that “high” describes “strain” and not “zone”.

    confirmed

## Remarks from the typesetter

Remarks from the typesetter

- TS1 Please check. Paul Bons or Paul D. Bons?

    'Paul D. Bons'

- TS2 Please provide the department.

    full affiliation including department:
    'School of Earth Science and Resources, China University of Geosciences, Beijing, China'

- TS3 The composition of Figs. 1, 6 and 9–11 has been adjusted to our standards.

    ok, thanks!

- TS4 Nye (1951) is not in the reference list. Do you mean Nye (1953)?

    it should be: 
    'Nye (1953)'

- TS5 Please check spelling: Glenn or Glen?

    should be:
    'Glen'

- TS6 Please check. There is only one Mansour et al. (2020) in the reference list.
please remove 'a, b' from citation, there is only one Mansour et al. (2020)

- TS7 Please confirm addition of citation.

    confirmed

- TS8 Bons et al. (2021) is not in the reference list.

    This is the missing citation: 
    Bons, P. D., de Riese, T., Franke, S., Llorens, M.-G., Sachau, T., Stoll, N., Weikusat, I., Westhoff, J. and Zhang, Y.: Comment on “Exceptionally High Heat Flux Needed to Sustain the Northeast Greenland Ice Stream” by Smith-Johnsen et al. (2020). The Cryosphere 15, 2251–54, https://doi.org/10.5194/tc-15-2251-2021, 2021.

- TS9 Please check spelling: Lillien or Lilien?

    'Lilien'

- TS10 Wolovick (2014) is not in the reference list. Do you mean Wolovick et al. (2014)?

    yes: Wolovick et al. (2014) 
    (also solves TS48)

- TS11 Moresi et al. (2006) is not in the reference list. Do you mean Moresi and Mühlhaus (2006)?

    Yes: Moresi and Mühlhaus (2006)
    (also solves TS37)

- TS12 Salles et al. (2016) is not in the reference list. Do you mean Salles et al. (2018)?

    Yes: Salles et al. (2018)
    (also solves TS43)

- TS13 Please provide date of last access.

    last access: 30 May 2022
    (also solves TS41) 

- TS14 Please check throughout the text that all vectors are denoted by bold italics and matrices by bold roman.

    TODO:

- TS15 Yang et al. (2020) is not in the reference list. Do you mean Yang et al. (2021)?

    'Yang et al. (2021)' is correct.
    (also solves TS51)

- TS16 Blatter et al. (1998) is not in the reference list.

    The missing citation:
    Blatter, H., Clarke, G. and Colinge, J.: Stress and velocity fields inglaciers: Part II.sliding and basal stress distribution. J. Glaciol., 44, 457–466, 1998.

- TS17 Pattyn et al. (2002) is not in the reference list.

    please change to: 
    'Pattyn et al. (2008)'

- TS18 Pattyn (2002) is not in the reference list.

    should be:
    'Pattyn et al. (2008)'

- TS19 Gagliardini et al. (2008) is not in the reference list. Do you mean Gagliardini and Zwinger (2008)?

    this should be:
    'Gagliardini and Zwinger (2008)'

- TS20 Gillet-Chaulet et al. (2005) is not in the reference list. Do you mean Gillet-Chaulet et al. (2006)?

    this should be:
    Gillet-Chaulet et al. (2006)

- TS21 Please note that units have been changed to exponential format throughout the text. Please check all instances.

    TODO:

- TS22 Gödert (2003) is not in the reference list.

    Missing reference:
    Gödert, G.: A Mesoscopic Approach for Modelling Texture Evolution of Polar Ice Including Recrystallization Phenomena, Annals of Glaciology 37, 23–28, https://doi.org/10.3189/172756403781815375, 2003.

- TS23 Please clarify whether the data set is your own. If yes, please provide a DOI in addition to your GitHub URL since our reference standard includes DOIs rather than URLs. If you have not yet created a DOI for your data set, please issue a Zenodo DOI (<https://help.github.com/en/github/creating-cloning-and-archiving-repositories/referencing-and-citing-content>). If the data set is not your own, please inform us accordingly. In any case, please ensure that you include a reference list entry corresponding to the data set including creators, title, and date of last access.

    TODO:
    ask Haibin to upload entire dataset (including **results**)

- TS24 Please clarify whether the data set is your own. If yes, please provide a DOI in addition to your GitHub URL since our reference standard includes DOIs rather than URLs. If you have not yet created a DOI for your data set, please issue a Zenodo DOI (<https://help.github.com/en/github/creating-cloning-and-archiving-repositories/referencing-and-citing-content>). If the data set is not your own, please inform us accordingly. In any case, please ensure that you include a reference list entry corresponding to the data set including creators, title, and date of last access.
 
    TODO:
    https://doi.org/10.5281/zenodo.1436039
    TODO:
    'Is this our own data set?' Not sure what to say here. Maybe remove the github link and replace with zenodo above.
    
    Corrales, L. 2015, dust: Calculate the intensity of dust scattering halos in the X-ray, v1.0, Zenodo, doi:10.5281/zenodo.15991

- TS25 Would you like to add any acknowledgements here?

    TODO:
    ask others

- TS26 Please note that the funding information has been added to this paper. Please check if it is correct. Please also double- check your acknowledgements to see whether repeated information can be removed or changed accordingly. Thanks.

    Please change text of the 'Funding' section to:
    'This open-access publication was funded by the Open Access Publishing Fund of the University of Tübingen.'

- TS27 Please ensure that any data sets and software codes used in this work are properly cited in the text and included in this reference list. Thereby, please keep our reference style in mind, including creators, titles, publisher/repository, persistent identifier, and publication year. Regarding the publisher/repository, please add "[data set]" or "[code]" to the entry (e.g. Zenodo [code]).

    TODO:
    check

- TS28 Please provide the complete author list.

    Bahadori, A., Holt. W. E., Feng, R., Austermann, J., Loughney, K. M., Salles, T., Moresi, L., Beucher, R., Lu, N., Flesch, L. M., Calvelage, C. M., Rasbury, E. T. Davis, D. M., Potochnik, A. R., Ward, W. B., Hatton, K., Haq, S. S. B., Smiley T. M., Wooton, K. M. and Badgley, C.

- TS29 Please add [code] or [data set].

    [code]

- TS30 Please confirm.

    confirmed

- TS31 Please provide the page range or article number.

    pages 6542-6548

- TS32 Drews et al. (2009) is not mentioned in the text.

    Please delete this citation

- TS33 Gogineni et al. (2014) is not mentioned in the text.

    Please delete this citation

- TS34 Please provide a persistent identifier.

    DOI: https://doi.org/10.1002/9780470750636.ch60

- TS35 Please provide the editors (if not authors) and a persistent identifier.

    Editor and author are identical

- TS36 Leng et al. (2012) is not mentioned in the text.

    Please delete citation

- TS37 Moresi and Mühlhaus (2006) is not mentioned in the text.

    It is mentioned now: see TS11

- TS38 Please provide the page range.

    pages: 69-82

- TS39 Please provide DOI.

    DOI: https://doi.org/10.1007/s00024-002-8737-4

- TS40 Please provide the page range or article number.

    page range: 2451–2463

    Full citation:
    Mühlhaus, H.-B., Moresi, L., and Cada, M.: Emergent Anisotropy and Flow Alignment in Viscous Rock, Pure Appl. Geophys., 161, 2451–2463, https://doi.org/10.1007/s00024-004-2575-5, 2004.

- TS41 Please provide date of last access.

    last access: 30 May 2022
    (see also TS13)

- TS42 Rignot and Mouginot (2012) is not mentioned in the text.

    Please remove citation

- TS43 Salles et al. (2018) is not mentioned in the text.

    It is mentioned now: see TS12

- TS44 Please provide the page range or article number.

    article number: e2019TC005894

    Full citation:
    Sandiford, D., Moresi, L. M., Sandiford, M., Farrington, R., and Yang, T.: The Fingerprints of Flexure in Slab Seismicity, Tectonics, 39, e2019TC005894, https://doi.org/10.1029/2019TC005894, 2020.

- TS45 Please provide the page range or article number.

    article number: e2019JF005252

    Full citation:
    Smith-Johnsen, S., Schlegel, N.-J., Fleurian, B., and Nisancioglu, K. H.: Sensitivity of the Northeast Greenland Ice Stream to Geothermal Heat, J. Geophys. Res.-Earth Surf., 125, e2019JF005252, https://doi.org/10.1029/2019JF005252, 2020.

- TS46 Please provide DOI.

    see TS47: citation deleted

- TS47 Weikusat et al. (2017) is not mentioned in the text.

    Please delete citation

- TS48 Wolovick et al. (2014) is not mentioned in the text.

    It is now mentioned: see TS10

- TS49 Please provide the page range or article number.

    Article number: 106637

- TS50 Yang et al. (2021) is not mentioned in the text.

    It is mentioned now: see TS15
