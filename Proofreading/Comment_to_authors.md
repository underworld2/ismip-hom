
# Comments for co-authors

- Paul
  - You are Paul D. Bons now (not just Paul Bons).
  - I changed your full affilation to 'School of Earth Science and Resources, China University of Geosciences, Beijing, China'. I hope this is correct.

- Haibin / Louis
  
  - Please include the results in the Zenodo repo, if possible. If not, it would be great if you would provide a dedicated repo. You will find them in the appendix.
  
  - Under TS25 they ask: 'Please clarify whether the data set is your own.' This referred to UW github repo. I replaced this now with the Zenodo-DOI (Beucher et al. and Mansour et al.) and hope that this resolves the issue.

- All
  - TS25 Would you like to add any acknowledgements?
    - I do not, everybody I'm grateful to is in the authors list.
    - Note that funding of the University of Tübingen is already acknowledged under the 'Financial support' section.
